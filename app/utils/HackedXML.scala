package utils

import scala.xml.{Elem, SAXParser}
import scala.xml.factory.XMLLoader

/**
  * Created by jpedro on 16.09.17.
  */
object HackedXML extends XMLLoader[Elem] {
  override def parser: SAXParser = {
    val f = javax.xml.parsers.SAXParserFactory.newInstance()
    f.setNamespaceAware(false)
    f.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
    f.newSAXParser()
  }
}
