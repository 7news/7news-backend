package controllers

import javax.inject._

import ml.SimpleArticleClusterMaker
import play.api._
import play.api.mvc._
import play.api.libs.json._
import play.api.libs.functional.syntax._
import reuters.{NewsArticle, ReutersNewsAPI, ReutersPipeline, Topic}


/**
 * This controller creates an `Action` to handle HTTP requests to the
 * application's home page.
 */
@Singleton
class HomeController @Inject()(cc: ControllerComponents) extends AbstractController(cc) {

  /**
   * Create an Action to render an HTML page.
   *
   * The configuration in the `routes` file means that this method
   * will be called when the application receives a `GET` request with
   * a path of `/`.
   */
  def index() = Action { implicit request: Request[AnyContent] =>
    val items = ReutersNewsAPI.getAllItems()

    Ok(items.map(_.prettyPrint).mkString("\n\n"))
  }

  def listTopics() = Action { implicit request: Request[AnyContent] =>
    //val items = ReutersPipeline.getNewsArticles(7)

    //println(items.map(_.prettyPrint).mkString("\n\n"))
    //val topics = items.slice(0, 7)

    val items = ml.GianApp.readFiles()

    val clusterMaker = new SimpleArticleClusterMaker
    val clusters = clusterMaker.cluster(items)
    clusters.foreach(println)

    val json = Json.toJson(clusters)

    Ok(json)
  }
}
