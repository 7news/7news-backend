package reuters

  import play.api.libs.functional.syntax._
  import play.api.libs.json._

  import scalaj.http.{Http, HttpOptions}

/**
  * Created by jpedro on 16.09.17.
  */
object ReutersSemanticAPI {

  private val APIKey: String = "fKtMbSPhMntcfgAz4O2qsEL2Y2mNqhub"

  private val BaseURL: String = "https://api.thomsonreuters.com/permid/calais"

  def main(args: Array[String]): Unit = {
    val someItem = ReutersNewsAPI.getAllItems().head
    println(enrichedNewsArticle(someItem))
  }

  def enrichedNewsArticle(reutersItem: ReutersItem): NewsArticle = {
    val textData = reutersItem.metaItem.headline + "\n" + reutersItem.paragraphs.mkString("\n")

    val responseText =
      Http(s"$BaseURL")
        .headers(
          ("Content-Type", "text/raw"),
          ("outputFormat", "application/json"),
          ("omitOutputtingOriginalText", "true"),
          ("X-AG-Access-Token", APIKey))
        .postData(textData).option(HttpOptions.connTimeout(10000))
        .asString
        .body

    val responseJson: JsValue = Json.parse(responseText)
    val topics = parseTopics(responseJson)
    val socialTags = parseSocialTags(responseJson)
    val industries = parseIndustries(responseJson)
    //val entities = parseEntities(responseJson)

    NewsArticle(reutersItem.metaItem.id, reutersItem.metaItem.headline, reutersItem.paragraphs,
                reutersItem.metaItem.dateCreated, reutersItem.metaItem.priority, reutersItem.categories,
                topics, socialTags, industries) //, entities
  }

  private def parseTopics(responseJson: JsValue): Vector[Topic] = {
    val allJsonObjects = responseJson.as[JsObject].value

    allJsonObjects
      .filter { case (_, jsValue) =>
        (jsValue \ "_typeGroup").toOption.map(_.as[JsString].value).getOrElse("") == "topics"
     }
      .map { case (_, jsValue) =>
        val topicName = (jsValue \ "name").get.as[JsString].value
        val topicScore = (jsValue \ "score").get.as[JsNumber].value.toDouble

        Topic(topicName, topicScore)
      }.toVector
  }

  private def parseSocialTags(responseJson: JsValue): Vector[SocialTag] = {
    val allJsonObjects = responseJson.as[JsObject].value

    allJsonObjects
      .filter { case (_, jsValue) =>
        (jsValue \ "_typeGroup").toOption.map(_.as[JsString].value).getOrElse("") == "socialTag"
      }
      .map { case (_, jsValue) =>
        val tagName = (jsValue \ "name").get.as[JsString].value
        val importance = (jsValue \ "importance").get.as[JsString].value.toInt

        SocialTag(tagName, importance)
      }.toVector
  }

  private def parseIndustries(responseJson: JsValue): Vector[Industry] = {
    val allJsonObjects = responseJson.as[JsObject].value

    allJsonObjects
      .filter { case (_, jsValue) =>
        (jsValue \ "_typeGroup").toOption.map(_.as[JsString].value).getOrElse("") == "industry"
      }
      .map { case (_, jsValue) =>
        val name = (jsValue \ "name").get.as[JsString].value

        Industry(name)
      }.toVector
  }

  private def parseEntities(responseJson: JsValue): Vector[Entity] = {
    val allJsonObjects = responseJson.as[JsObject].value

    allJsonObjects
      .filter { case (_, jsValue) =>
        (jsValue \ "_typeGroup").toOption.map(_.as[JsString].value).getOrElse("") == "entities"
      }
      .map { case (_, jsValue) =>
        val _type = (jsValue \ "_type").get.as[JsString].value
        val name = (jsValue \ "name").get.as[JsString].value
        val entityScore = (jsValue \ "confidencelevel").getOrElse(JsString(Double.NaN.toString)).as[JsString].value.toDouble

        val entity = _type match {
          case "Person" =>
            val personType = (jsValue \ "persontype").get.as[JsString].value
            val nationality = (jsValue \ "nationality").get.as[JsString].value
            Person(name, personType, entityScore, nationality)

          case "Organization" =>
            val orgType = (jsValue \ "organizationtype").get.as[JsString].value
            val nationality = (jsValue \ "nationality").get.as[JsString].value
            Organization(name, orgType, entityScore, nationality)

          case other: String =>
            new Entity(other, name, entityScore)
        }

        entity
      }.toVector
  }

}

case class Topic(name: String, confidence: Double)
object Topic {
  implicit val topicWrites: Writes[reuters.Topic] = Json.writes[Topic]
  implicit val topicReads: Reads[reuters.Topic] = Json.reads[Topic]
}
case class SocialTag(name: String, importance: Int)
object SocialTag {
  implicit val topicWrites: Writes[reuters.SocialTag] = Json.writes[SocialTag]
  implicit val topicReads: Reads[reuters.SocialTag] = Json.reads[SocialTag]
}

case class Industry(name: String)
object Industry {
  implicit val topicWrites: Writes[reuters.Industry] = Json.writes[Industry]
  implicit val topicReads: Reads[reuters.Industry] = Json.reads[Industry]
}

class Entity(val _type: String, val name: String, val confidence: Double) {
  override def toString: String = s"Entity(${_type}, $name, $confidence)"
}

object Entity {

  def unapply(arg: Entity): Option[(String, String, Double)] = Some((arg._type, arg.name, arg.confidence))

  def apply(_type: String, name: String, confidence: Double): Entity = new Entity(_type, name, confidence)

  implicit val topicWrites: Writes[reuters.Entity] = (
    (JsPath \ "_type").write[String] and
      (JsPath \ "name").write[String] and
      (JsPath \ "confidence").write[Double]
    ) (unlift(reuters.Entity.unapply))

  implicit val entityReads: Reads[Entity] = (
    (JsPath \ "_type").read[String] and
      (JsPath \ "name").read[String] and
      (JsPath \ "confidence").read[Double]
    )(Entity.apply _)
}

case class Person(override val name: String, personType: String, override val confidence: Double, nationality: String)
  extends Entity("Person", name, confidence)

case class Organization(override val name: String, organizationType: String, override val confidence: Double, nationality: String)
  extends Entity("Organization", name, confidence)

case class NewsArticle(id: String,
                       title: String,
                       paragraphs: Vector[String],
                       dateCreated: String,
                       priority: Int,
                       categories: Vector[String],
                       topics: Vector[Topic],
                       socialTags: Vector[SocialTag],
                       industries: Vector[Industry]
                       //entities: Vector[Entity]
                      ) {
  def prettyPrint: String = {
    s"""
       |{
       |TITLE: ${title}
       |
       |CATEGORIES: ${categories.mkString(", ")}
       |ID: ${id}
       |DATE_CREATED: ${dateCreated}
       |PRIORITY: ${priority}
       |TOPICS: ${topics.mkString(", ")}
       |SOCIALTAGS: ${socialTags.mkString(", ")}
       |INDUSTRIES: ${industries.mkString(", ")}
       |
       |PARAGRAPHS:
       |
       |${paragraphs.mkString("\n")}
       |
       |}
    """.stripMargin
  }
  //ENTITIES: ${entities.mkString(", ")}
}

object NewsArticle {
  implicit val topicWrites: Writes[reuters.NewsArticle] = Json.writes[NewsArticle]
  implicit val topicReads: Reads[reuters.NewsArticle] = Json.reads[NewsArticle]
}