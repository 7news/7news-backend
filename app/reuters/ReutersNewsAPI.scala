package reuters

import utils.HackedXML

import scala.xml.XML
import scalaj.http.{Http, HttpOptions}

/**
  * Created by jpedro on 16.09.17.
  */
object ReutersNewsAPI {

  private val APIKey = "0Uar2fCpykWj7ZFgF2rU53CEkYMcqTyb81kIX5wuiTI="

  private val BaseURL = "http://rmb.reuters.com/rmd/rest/xml"

  object Channels {

    val CoreNews: String = "Wbz248"
    val WorldService: String = "STK567"
    val WorldNews: String = "BEQ259"

  }

  def getAllItems(limit: Int = 300, channel: String = Channels.WorldNews): Vector[ReutersItem] = {
    getAllMetaItemsForChannel(channel).take(limit).map(getItem)
  }

  def getAllMetaItemsForChannel(channel: String): Vector[ReutersMetaItem] = {
    val xmlMetaItemsStr = Http(s"$BaseURL/items?channel=$channel&mediaType=T&token=$APIKey").option(HttpOptions.connTimeout(10000)).asString.body
    val xmlMetaItems = HackedXML.loadString(xmlMetaItemsStr)

    (xmlMetaItems \ "result").map { xmlRes =>
      val id = (xmlRes \ "id").text
      val dateCreated = (xmlRes \ "dateCreated").text
      val headline = (xmlRes \ "headline").text
      val priority = (xmlRes \ "priority").text.toInt

      ReutersMetaItem(id, dateCreated, headline, priority)
    }.toVector
  }

  def getItem(metaItem: ReutersMetaItem): ReutersItem = {
    val xmlItemStr = Http(s"$BaseURL/item?id=${metaItem.id}&token=$APIKey").option(HttpOptions.connTimeout(10000)).asString.body
    val xmlItem = HackedXML.loadString(xmlItemStr)
    val categories =
      (xmlItem \\ "memberOf").map(x => (x \ "name").text)

    val paragraphs = (xmlItem \\ "body" \ "p").map(p => p.text)

    ReutersItem(metaItem, categories.toVector, paragraphs.toVector)
  }


}

case class ReutersMetaItem(id: String, dateCreated: String, headline: String, priority: Int)

case class ReutersItem(metaItem: ReutersMetaItem, categories: Vector[String], paragraphs: Vector[String]) {

  def prettyPrint: String = {
    s"""
       |{
       |TITLE: ${metaItem.headline}
       |
       |CATEGORIES: ${categories.mkString(", ")}
       |ID: ${metaItem.id}
       |DATE_CREATED: ${metaItem.dateCreated}
       |PRIORITY: ${metaItem.priority}
       |
       |PARAGRAPHS:
       |
       |${paragraphs.mkString("\n")}
       |
       |}
    """.stripMargin
  }

}