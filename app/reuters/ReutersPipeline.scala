package reuters

import ml.SimpleArticleClusterMaker

/**
  * Created by jpedro on 16.09.17.
  */
object ReutersPipeline {

  /**
    * Returns up to n NewsArticles from Reuters.
    *
    * @param n limit of number of articles
    * @return up to n NewsArticles from Reuters enriched with entities, topics, etc.
    */
  def getNewsArticles(n: Int): Vector[NewsArticle] = {
    ReutersNewsAPI.getAllItems(n).map(i => ReutersSemanticAPI.enrichedNewsArticle(i))
  }

  def main(args: Array[String]): Unit = {
    val items = ReutersPipeline.getNewsArticles(100).groupBy(_.title).map(_._2.head).toVector
    //println(items.map(_.prettyPrint).mkString("\n\n"))
    //val topics = items.slice(0, 7)

    val clusterMaker = new SimpleArticleClusterMaker
    val clusters = clusterMaker.cluster(items)
    clusters.foreach(println)
  }
}
