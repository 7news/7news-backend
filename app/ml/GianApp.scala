package ml

import reuters.ReutersSemanticAPI.enrichedNewsArticle
import reuters.{NewsArticle, ReutersNewsAPI, ReutersPipeline, ReutersSemanticAPI}
import java.io._
import play.api.libs.json._
import play.api.libs.functional.syntax._

object GianApp {
  val path = "/Users/mw/Temp/7news-data/"

  def generateText(art: NewsArticle): String = {
    /*
    var txt = ""
    for(i <- 0 to art.paragraphs.length-1) {
      txt += art.paragraphs(i)
      txt += " \n"
    }
    txt += "\nTAGSOFARTICLE\n"
    txt += art.title + "\n"
    txt += art.socialTags.mkString(" ") + "\n"
    txt += art.priority + "\n"
    txt += art.dateCreated + "\n"
    txt += art.categories.mkString(" ") + "\n"
    // txt += art.entities.mkString(" ") + "\n"
    txt += art.topics.mkString(" ") + "\n"
    txt += art.industries.mkString(" ") + "\n"
    txt
    */
    Json.toJson(art).toString()
  }

  def writeFile(art: NewsArticle, filename: String): Unit = {
    val txt = generateText(art)

    val pw = new PrintWriter(new File(path+filename+".txt" ))
    pw.write(txt)
    pw.close
  }

  def readFile(file: File): NewsArticle = {
    Json.fromJson[NewsArticle](Json.parse(scala.io.Source.fromFile(file).mkString)).get
  }

  def readFiles(): Vector[NewsArticle] = {
    val list = new java.io.File(path).listFiles().filter(_.getName.endsWith(".txt"))
    list.toVector.map(text => readFile(text))
  }

  def main(args: Array[String]): Unit = {

    //val data = ReutersPipeline.getNewsArticles(10)
    //data.foreach(article => writeFile(article, article.id))

    val articles = readFiles()
    println(articles.mkString("\n"))

    /*
    for(i <- 0 to (data.length-1)) {
      val data1 = data(i)
      val txt = data1.toJson()//generateText(data1)
      val pw = new PrintWriter(new File(path+data1.id+".txt" ))
      pw.write(txt)
      pw.close
//    }
*/
    }
}

class GianArticleClusterMaker extends AbstractArticleClusterMaker {

  override def cluster(articles: Vector[NewsArticle]): Vector[ArticleCluster] = {
    Vector(ArticleCluster("all", articles))
  }

}