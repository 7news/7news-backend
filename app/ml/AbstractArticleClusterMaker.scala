package ml

import reuters.NewsArticle
import play.api.libs.json._

/**
  * Created by jpedro on 16.09.17.
  */
abstract class AbstractArticleClusterMaker {

  def cluster(articles: Vector[NewsArticle]): Vector[ArticleCluster]

}

abstract class AbstractArticleRanker {

  def rank(articles: Vector[NewsArticle]): Vector[NewsArticle]

}

abstract class AbstractArticleSummarizer {

  def summarize(articles: Vector[NewsArticle]): String

}

case class ArticleCluster(description: String, articles: Vector[NewsArticle])
object ArticleCluster {
  implicit val articleClusterWrites: Writes[ArticleCluster] = Json.writes[ArticleCluster]
  implicit val articleClusterReads: Reads[ArticleCluster] = Json.reads[ArticleCluster]
}
