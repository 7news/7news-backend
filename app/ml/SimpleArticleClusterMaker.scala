package ml
import reuters.NewsArticle

import scala.collection.mutable

/**
  * Created by jpedro on 16.09.17.
  */
class SimpleArticleClusterMaker extends AbstractArticleClusterMaker {
  private val MaxNumberOfClusters = 7
  private val MaxProportion = 1 / 5.0

  override def cluster(articles: Vector[NewsArticle]): Vector[ArticleCluster] = {
    var alreadySelectedArticles: Set[NewsArticle] = Set.empty

    val articlesByTopic =
      articles.flatMap(a => a.socialTags.map(t => (t, a)))
        .groupBy(_._1.name)
        .mapValues(v => v.map(_._2).toSet)

    // take the topics from all the articles
    var mutableArticlesByTopic = articlesByTopic.filter(_._2.size <= MaxProportion * articles.size)

    var clusters = Vector.empty[ArticleCluster]
    var selectedTopics = Vector.empty[String]

    while(clusters.size < Math.min(mutableArticlesByTopic.size, MaxNumberOfClusters) && alreadySelectedArticles.size < articles.size) {
      // rank them by most frequent
      // take one, remove all the counts from topics that appear together
      // take the next most frequent
      val (bestTopic, topicArticles) =
        mutableArticlesByTopic.filter(tup => !selectedTopics.contains(tup._1)).toVector.maxBy(_._2.size)
      alreadySelectedArticles ++= topicArticles
      val sortedSubTopics = topicArticles.flatMap(_.socialTags.map(_.name)).groupBy(identity).map(t => (t._1, -t._2.size)).toVector.sortBy(_._2).unzip._1
      val subTopics = sortedSubTopics.filter(v => !selectedTopics.contains(v) && v != bestTopic).take(3)
      val description = if(subTopics.nonEmpty) s"$bestTopic: ${subTopics.mkString(", ")}" else bestTopic
      clusters +:= ArticleCluster(description, articlesByTopic(bestTopic).toVector)
      selectedTopics +:= bestTopic
      mutableArticlesByTopic = mutableArticlesByTopic.mapValues(v => v diff topicArticles)
    }

    clusters.reverse
  }

}
