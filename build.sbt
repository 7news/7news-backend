name := """seven-news"""
organization := "com.7news"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.12.3"

libraryDependencies += guice
libraryDependencies += "org.scalaj" %% "scalaj-http" % "2.3.0"
